# -*- coding: UTF-8 -*-

from django.contrib import admin
from tracking.models import Tracking, TrackingEvent

class TrackingEventInline(admin.StackedInline):
  model = TrackingEvent
  extra = 0

class TrackingAdmin(admin.ModelAdmin):
  list_display = ['tracking_id',]
  inlines = [TrackingEventInline,]
admin.site.register(Tracking, TrackingAdmin)

admin.site.register(TrackingEvent)
