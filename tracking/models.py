# -*- coding: UTF-8 -*-

from django.db import models

class Tracking(models.Model):
  tracking_id = models.CharField(max_length=255)

  added_on = models.DateTimeField(auto_now_add=True)
  modified_on = models.DateTimeField(auto_now=True)

  def update(self):
    from tracking.parsers.itella import get_tracking_data

    data = get_tracking_data(self.tracking_id)

    for event in data['events']:
      object = TrackingEvent(tracking=self)
      object.description = event['name']
      object.location = event['Paikka:']
      object.time = event[u'Rekisteröinti:']
      object.save()

  def __unicode__(self):
    return self.tracking_id

class TrackingEvent(models.Model):
  tracking = models.ForeignKey(Tracking)

  description = models.CharField(max_length=255)
  location = models.CharField(max_length=255)
  time = models.DateTimeField()

  added_on = models.DateTimeField(auto_now_add=True)
  modified_on = models.DateTimeField(auto_now=True)

  def __unicode__(self):
    return self.description
