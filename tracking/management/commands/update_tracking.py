# -*- coding: UTF-8 -*-

from django.core.management.base import BaseCommand, CommandError
from tracking.models import Tracking

class Command(BaseCommand):
  help = 'Updates information from tracking'

  def handle(self, *args, **options):
    trackings = Tracking.objects.all()
    for tracking in trackings:
      tracking.update()
