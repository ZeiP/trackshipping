import urllib2
from lxml import etree
from datetime import datetime

from tracking.models import Tracking

def get_tracking_data(id, lang='fi'):
  url = 'http://www.posti.fi/itemtracking/posti/search_by_shipment_id?lang=' + lang + '&ShipmentId=' + id

  data = urllib2.urlopen(url).read()

  tree = etree.HTML(data)

  rows = tree.xpath('//table[@id="shipment-event-table"]/tbody/tr/td/div[@id="shipment-event-table-cell"]')

  data = {}
  data['id'] = id
  data['events'] = []

  for row in rows:
    event = {}
    event['name'] = row.xpath('div[@class="shipment-event-table-header"]/text()')[0]

    details = row.xpath('div[@class="shipment-event-table-row"]')

    for detail in details:
      label = detail.xpath('span[@class="shipment-event-table-label"]/text()')
      if not label:
        continue
      else:
        label = label[0]

      content_str = detail.xpath('span[@class="shipment-event-table-data"]/text()')
      if not content_str:
        continue
      else:
        content_str = content_str[0]

      try:
        content = datetime.strptime(content_str, '%d.%m.%Y klo %H:%M:%S')
      except ValueError:
        content = content_str

      event[label] = content

    data['events'].append(event)

  return data
