## Introduction ##

TrackShipping is a simple web-based application to track parcels via different delivery operators.
It currently has a parser for Itella, but others will come. The UI is also quite non-existant so far,
needs a bit of work :)

## Running it ##


```
#!sh

virtualenv env
source env/bin/activate
pip install -r pip-req.txt
./manage.py syncdb
./manage.py migrate tracking
./manage.py runserver
```
